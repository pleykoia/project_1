package ru.rtkit.someservice.tests.model;


import lombok.Data;

@Data
public class Order {

    Integer id;
    Integer petId;
    String status;
    Integer quantity;
    Boolean complete;
    String shipDate;

    public Integer getQuantity() {
        return quantity;
    }

    public Integer getId() {
        return id;
    }

    public Integer getPetId() {
        return petId;
    }

    public String getStatus() {
        return status;
    }

    public Boolean getComplete() {
        return complete;
    }

    public String getShipDate() {
        return shipDate;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPetId(Integer petId) {
        this.petId = petId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

    public void setShipDate(String shipDate) {
        this.shipDate = shipDate;
    }


}
