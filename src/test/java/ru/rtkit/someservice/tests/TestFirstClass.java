package ru.rtkit.someservice.tests;

import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.rtkit.someservice.tests.apiHelper.EndPoints;

public class TestFirstClass extends BaseTest {

    @Test
    @DisplayName("тест добавление питомца + проверка")
    void TestAddNewPetInStore() {

        // формируем тело запроса с использованием объекта JSONObject
        JSONObject body = new JSONObject()
                .put("id", 5)
                .put("name", "Буcя")
                .put("status", "available");
        // добавление питомца в магазин
        String id = api.post(EndPoints.NEW_PET, body.toString(), resp200)
                .jsonPath()
                .getString("id");

        //проверка что питомец в магазине
        api.get(EndPoints.PET_ID, resp200, id)
                .jsonPath()
                .getString("id");
    }

    @Test
    @DisplayName("тест обновление питомца + проверка")
    void TestUpdatePetInStore() {

        JSONObject bodyJO = new JSONObject()
                .put("id", 5)
                .put("name", "Муся")
                .put("status", "available");
        // обновление питомца в магазине
        String newPet = api.put(EndPoints.NEW_PET, bodyJO.toString(), resp200)
                .jsonPath()
                .getString("id");


        //проверка что питомец в магазине
        api.get(EndPoints.PET_ID, resp200, newPet)
                .jsonPath()
                .getString("id");
    }

    @Test
    @DisplayName("тест удаление питомца + проверка")
    void TestDeletePetInStore() {

        // получение id питомца
        String delPetId = api.get(EndPoints.PET_ID, resp200, 5)
                .jsonPath()
                .getString("id");
        // удаление питомца в магазине
        api.delete(EndPoints.PET_ID, resp200, delPetId);

        //проверка что питомца в магазине нет
        api.get(EndPoints.PET_ID, resp404, delPetId)
                .jsonPath()
                .getString("id");
    }
}
