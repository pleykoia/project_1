package ru.rtkit.someservice.tests;

import io.restassured.specification.ResponseSpecification;
import ru.rtkit.someservice.tests.apiHelper.PetStoreApi;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.expect;

public class BaseTest {
    public static ResponseSpecification resp200 = expect().statusCode(200);
    public static ResponseSpecification resp404 = expect().statusCode(404);
    public static PetStoreApi api;

    public static Properties props;
    static {
        InputStream iURI = ClassLoader.getSystemResourceAsStream( "service.properties");
        props = new Properties();
        try {
            props.load(iURI);
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        api = new PetStoreApi();
    }

}
