package ru.rtkit.someservice.tests;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import jdk.jfr.Description;
import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import ru.rtkit.someservice.tests.apiHelper.EndPoints;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("класс для добавления кучи питомцев")
public class TestHamcrest extends BaseTest {
    @ParameterizedTest(name = "{index} - {0}")
    @CsvSource({
            "5, 'Busya'",
            "6, 'BUSYA'",
            "7, 'busya'",
            "8, 'BuSyA'",
            "9, 'Буся'",
            "10, 'БУСЯ'",
            "11, 'буся'",
            "12, 'БуСя'"
    })
    @DisplayName("тест для добавления кучи питомцев")
    @Description("проверка клички в любом регистре и на любых языках")
    void testAddPet(Integer id, String name) {
        String a = "буся";
        String b = "busya";
        JSONObject bodyJO = new JSONObject()
                .put("id", id)
                .put("name", name)
                .put("status", "available");

        long petId = addNewPetToStore(bodyJO.toString(), name);
        String realName = api.get(EndPoints.PET_ID, resp200, petId)
                .jsonPath()
                .getString("name");
        Assertions.assertEquals(name, realName, "Ошибка при проверке имени созданного животного");
    }

    @Step("Добавление питомцев в магазин")
    long addNewPetToStore(String pet, String petNames) {
        return api.post(EndPoints.NEW_PET, pet, resp200)
                .jsonPath()
                .getLong("id");
    }

    @Test
    @DisplayName("тест для заказа питомца")
    void testOrderPet() {

        String pl = "placed";
        String ap = "approved";
        String del = "delivered";
        int orderId = 8;
        LocalDate date = LocalDate.now();

        //заказ питомца
        JSONObject bodyJO = new JSONObject()
                .put("petId", orderId)
                .put("status", "placed")
                .put("complete", "true")
                .put("shipDate", date);
        String orderNew = api.post(EndPoints.NEW_ORDER, bodyJO.toString(), resp200)
                .jsonPath()
                .getString("id");

        //проверка что создан заказ для питомца
        Response response = api.get(EndPoints.ORDER_ID, resp200, orderNew);

        //переменные для assert
        String orderNewId = response.jsonPath().getString("id");
        String shipDate = response.jsonPath().getString("shipDate");
        String status = response.jsonPath().getString("status");
        boolean complete = response.jsonPath().getBoolean("complete");

        //сравнение
        assertEquals(orderNew, orderNewId);
        assertTrue(complete);
        assertNotNull(shipDate);
        assertThat(status, Matchers.anyOf(Matchers.equalToIgnoringCase(pl), Matchers.equalToIgnoringCase(ap), Matchers.equalToIgnoringCase(del)));
    }
}
