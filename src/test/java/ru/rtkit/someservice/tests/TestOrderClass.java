package ru.rtkit.someservice.tests;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.rtkit.someservice.tests.apiHelper.EndPoints;
import ru.rtkit.someservice.tests.model.Order;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static ru.rtkit.someservice.tests.BaseTest.api;
import static ru.rtkit.someservice.tests.BaseTest.resp200;

public class TestOrderClass {

    @Test
    @DisplayName("Забронировать питомца")
    void testPutOrderPet() {
        String pl = "placed";
        String ap = "approved";
        String del = "delivered";
        Integer id = 9;
        Integer orderId = 8;
        LocalDate date = LocalDate.now();
        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String DateToString = date.format(f);

        Order orderReq = new Order();
        orderReq.setId(id);
        orderReq.setPetId(orderId);
        orderReq.setStatus(pl);
        orderReq.setComplete(true);
        orderReq.setShipDate(DateToString);

        api.post(EndPoints.NEW_ORDER, orderReq, resp200);
        Order OrderPet = api.get(EndPoints.ORDER_ID, resp200, orderReq.getId()).as(Order.class);

        //сравнение
        assertEquals(orderReq.getPetId(), OrderPet.getPetId());
        assertTrue(orderReq.getComplete());
        assertNotNull(orderReq.getShipDate());
        assertThat(orderReq.getStatus(), Matchers.anyOf(Matchers.equalToIgnoringCase(pl), Matchers.equalToIgnoringCase(ap), Matchers.equalToIgnoringCase(del)));
    }
}

