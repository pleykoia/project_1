package ru.rtkit.someservice.tests.apiHelper;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;
import ru.rtkit.someservice.tests.BaseTest;

import static io.restassured.RestAssured.given;

public class PetStoreApi {
    private static final String URI = BaseTest.props.getProperty("baseUrl");

    public PetStoreApi() {
        RestAssured.baseURI = URI;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.filters(new AllureRestAssured());

    }

    public Response get(String endpoint, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }

    public Response get(String endpoint, ResponseSpecification resp, Object pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint, pathParams);
    }

    public Response post(String endpoint, Object body, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .post(endpoint);
    }

    public Response put(String endpoint, Object body, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .put(endpoint);
    }

    public Response put(String endpoint, Object body, Object pathParams, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .put(endpoint, pathParams);
    }

    public Response delete(String endpoint, ResponseSpecification resp, Object pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .header("api_key", "api_key")
                .expect()
                .spec(resp)
                .when()
                .delete(endpoint, pathParams);
    }
}
