package ru.rtkit.someservice.tests.apiHelper;

public class EndPoints {
    //pet
    public static final String NEW_PET = "/pet";
    public static final String PET_ID = "/pet/{id}";

    //store
    public static final String INVENTORY = "/store/inventory";
    public static final String NEW_ORDER = "/store/order";
    public static final String ORDER_ID = "/store/order/{id}";

    //user
    public static final String USER = "/user";
    public static final String USER_NAME = "/user/{username}";
    public static final String USER_LOGIN = "/user/login";
}
