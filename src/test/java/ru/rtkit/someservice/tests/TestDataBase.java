package ru.rtkit.someservice.tests;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TestDataBase {
    String pathToDB = Objects.requireNonNull(getClass().getClassLoader().getResource("sqlite/chinook.db")).getPath();
    String dbUrl = "jdbc:sqlite:" + pathToDB;

    @Test
    @DisplayName("вывести одну строку из БД")
    void get() {
        String query = "SELECT * FROM Tracks LIMIT 1 ";
        List<Track> tracks = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Track track1 = new Track(
                    rs.getLong("trackId"),
                    rs.getString("name"),
                    rs.getLong("albumId"),
                    rs.getLong("mediaTypeId"),
                    rs.getLong("genreId"),
                    rs.getString("composer"),
                    rs.getLong("milliseconds"),
                    rs.getLong("bytes"),
                    rs.getDouble("unitPrice"));
                tracks.add(track1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println(tracks);
    }

    @DisplayName("получить id жанра поп")
    private List<String> getGenres() {
        String query = "SELECT GenreId FROM genres WHERE Name = 'Pop'";
        List<String> genres = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                genres.add(rs.getString("GenreId"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println("\n Pop genreId:"+ genres);
        return genres;
    }

    @Test
    @DisplayName("вывести все треки из БД с жанром поп. Lombok")
    void getAllPopTracks() {
            List<String> genres = getGenres();
            String query = "SELECT * FROM Tracks WHERE GenreId in ("
                    + genres.stream().collect(Collectors.joining(", "))
                    + ")";
            List<Track> tracks = new ArrayList<>();
            try (Connection conn = DriverManager.getConnection(dbUrl);
                 PreparedStatement ps = conn.prepareStatement(query);
                 ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Track track = new Track(
                            rs.getLong("trackId"),
                    rs.getString("name"),
                    rs.getLong("albumId"),
                    rs.getLong("mediaTypeId"),
                    rs.getLong("genreId"),
                    rs.getString("composer"),
                    rs.getLong("milliseconds"),
                    rs.getLong("bytes"),
                    rs.getDouble("unitPrice")
                    );
                    tracks.add(track);
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            System.out.println("\nTrack list:");
            tracks.forEach(System.out::println);
    }

    @Data
    @AllArgsConstructor
    private static class Track {
        Long trackId;
        String name;
        Long albumId;
        Long mediaTypeId;
        Long genreId;
        String composer;
        Long milliseconds;
        Long bytes;
        Double unitPrice;

        @Override
        public String toString() {
            return "{" +
                    "trackId=" + trackId +
                    ", name='" + name + '\'' +
                    ", albumId=" + albumId +
                    ", mediaTypeId=" + mediaTypeId +
                    ", genreId=" + genreId +
                    ", composer='" + composer + '\'' +
                    ", milliseconds=" + milliseconds +
                    ", bytes=" + bytes +
                    ", unitPrice=" + unitPrice +
                    '}';
        }
    }
}
